package ast;

import compiler.CodeBlock;

public class ASTAnd implements ASTNode {

	ASTNode left, right;
	
	public ASTAnd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public int eval() {
		if (left.eval() > 0 && right.eval() > 0) {
			return 1;
		}
		return 0;
	
	}
	
	@Override
	public String toString() {
		return left.toString() + " && " + right.toString();
	}

	@Override
	public void compile(CodeBlock code) {
		this.left.compile(code);
		this.right.compile(code);
		code.emit_div();
		
	}
}
