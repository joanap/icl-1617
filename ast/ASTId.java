package ast;
import compiler.CodeBlock;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTId implements ASTNode {

	String id;

	public ASTId(String id)
	{
		this.id = id;
	}

	public int eval(Environment<Integer> env) 
			throws UndeclaredIdentifierException, ExecutionErrorException { 
		return env.find(id); 
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}
	
}

